/** User     :双日新新都市開発株式会社
 *  System   :販売管理システム
 *  App      :自社マスタ
 *  Function :画面制御
 *  Create   :2018.03.23 by fukui
 *  **********************************************************
 *  Update   :YYYY.MM.DD modifier 
 *  **********************************************************
 * 物件マスタ              GET PUT
 * 協力会社様向け受注管理    GET PUT
 */

//--------------------------------------
// 広域変数宣言
//--------------------------------------

jQuery.noConflict();
(function($) {
    "use strict";

	//------------------------------------------------------
    // 定数宣言
	//------------------------------------------------------
var dbg_flg = 1;
//var dbg_flg = 0;
	const EVENT_INDEX_SHOW = ['app.record.index.show'];
	const EVENT_DETAIL_SHOW = ['app.record.detail.show'];
	
	const EVENT_INDEX_EDIT_SHOW = ['app.record.index.edit.show'];
	const EVENT_CREATE_EDIT_SHOW = ['app.record.create.show','app.record.edit.show'];
	
	const EVENT_INDEX_EDIT_SUBMIT = ['app.record.index.edit.submit'];
	const EVENT_CREATE_EDIT_SUBMIT = ['app.record.create.submit','app.record.edit.submit'];

	const EVENT_INDEX_EDIT_SUCCESS = ['app.record.index.edit.submit.success'];
	const EVENT_CREATE_EDIT_SUCCESS = ['app.record.create.submit.success','app.record.edit.submit.success'];

	const EVENT_INDEX_DELETE_SUBMIT = ['app.record.index.delete.submit'];
	const EVENT_DETAIL_DELETE_SUBMIT = ['app.record.detail.delete.submit'];
	
	const EVENT_PROCESS_PROCEED = ['app.record.detail.process.proceed'];
	
	//------------------------------------------------------
	// 外部変数宣言
	//------------------------------------------------------

	//------------------------------------------------------
	// 関連アプリ更新
	//  function : updapp_self
	//  @event   : 画面表示情報
	//  @appid   : 更新対象アプリID
	//  return   : promise
	//------------------------------------------------------
	function updapp_self(event, appid){
		//--------------------------------------
		// 更新条件生成
		//--------------------------------------
		var rec_id = event['record']['$id']['value'];
		var str_query = '';
		
		var guest_flg = false;
		
		switch(appid){
		case kintone.app.getLookupTargetAppId('lup_condo_cd'): // 物件マスタ
			str_query = 'lup_self=\"' + rec_id + '\"';
			break;
		case kintone.app.getLookupTargetAppId('lup_m_recno'): // 協力会社様向け受注管理
			str_query = 'lup_self=\"' + rec_id + '\"';
			break;
		}

		//--------------------------------------
		// 関連アプリ情報取得
		//--------------------------------------
		var param = {
			app: appid,
			query: str_query,
			fields: ['$id', 'lup_self'],
			totalCount: true,
			isGuest: guest_flg
		};
		
		return kintoneUtility.rest.getAllRecordsByQuery(param).then(function(get_resp) {
			// success
			
			// 更新情報生成
			var recs = get_resp.records;
			var recs_cnt = recs.length;
			var records = [];

			// 件数チェック
			if(recs_cnt === 0){
				return ;
			}
			
			for(var i = 0; i < recs_cnt; i++){
				var rec = recs[i];
				var wk_rec = {};
				var wk_buf = {};
				wk_rec['id'] = rec['$id']['value'];
				wk_buf['lup_self'] = {value:rec['lup_self']['value']}; 
				wk_rec['record'] = wk_buf;
				records.push(wk_rec);
			}
			
			var param = {
				app: appid,
				records: records,
				isGuest: guest_flg
			};
		
			//--------------------------------------
			// 関連アプリ情報更新
			//--------------------------------------
			kintoneUtility.rest.putAllRecords(param).then(function(put_resp) {
				// success
			}).catch(function(put_error) {
				// error
				console.log(put_error.message);
			});
		}).catch(function(get_error) {
			// error
			console.log(get_error.message);
		});		
	}
	
	/* function  : 郵便番号→住所取得
	 * @event    : true/表示 false/非表示
	 * return    : void
	 */
	function set_address(event, zip_field){
		// 住所情報設定項目
		var btn_arr = {};
		// 自社住所
		btn_arr['sp_self_zip'] = {
			'zip' : 'txt_self_zip',
			'add' : 'txt_self_add1'
		};
		var rec = kintone.app.record.get();
		var fl_zip = btn_arr[zip_field]['zip'];
		var fl_add = btn_arr[zip_field]['add'];
		var zipcode = rec['record'][fl_zip]['value'];
			
		if (!zipcode || !zipcode.match(/^[0-9]{3}\-?[0-9]{4}$/)) {
			rec['record'][fl_zip].error = '7桁の半角数字で入力して下さい。';
			kintone.app.record.set(rec);
			return;
		}
		var endpoint = 'https://madefor.github.io/postal-code-api/api/v1';
		var code1 = zipcode.replace(/^([0-9]{3}).*/, "$1");
		var code2 = zipcode.replace(/.*([0-9]{4})$/, "$1");
		kintone.proxy(endpoint + '/' + code1 + '/' + code2 + '.json', 'GET', {}, {}).then(function (args) {
			//success
			/*  args[0] -> body(文字列)
			 *  args[1] -> status(数値)
			 *  args[2] -> headers(オブジェクト)
			 */
			if (args[1] == 200) {
				//success
				var resp = JSON.parse(args[0]);
//				rec.record.zipcode.value = code1 + '-' + code2;
				rec['record'][fl_zip].error = null;
				rec['record'][fl_add].value = resp.data[0].ja.prefecture + resp.data[0].ja.address1 + resp.data[0].ja.address2 + resp.data[0].ja.address3;
//				rec.record.company.value = resp.data[0].ja.address4;
				kintone.app.record.set(rec);
				return;
			} else {
				//error
				alert('郵便番号から住所の検索に失敗しました。');
				console.log(args[1], args[0], args[2]);
				return;
			}
		}, function (error) {
			//error
			alert('郵便番号から住所の検索に失敗しました。');
			console.log(error); //proxy APIのレスポンスボディ(文字列)を表示
			return;
		});
	}
	
	/* function  : フィールド表示/非表示
	 * @show_flg : true/表示 false/非表示
	 * return    : void
	 */
	function set_fieldshown(show_flg){
		kintone.app.record.setFieldShown('grp_admin', show_flg);
	}

	//--------------------------------------
	// 保存完了時制御
	//--------------------------------------
	kintone.events.on(EVENT_CREATE_EDIT_SUCCESS, function(event) {
        updapp_self(event, kintone.app.getLookupTargetAppId('lup_condo_cd')); // 物件マスタ
        updapp_self(event, kintone.app.getLookupTargetAppId('lup_m_recno')); // 協力会社様向け受注管理

		return event;
		
	});
	
	//--------------------------------------
	// 保存ボタン押下時制御
	//--------------------------------------
	kintone.events.on(EVENT_CREATE_EDIT_SUBMIT, function(event) {
		return event;
		
	});
	
	//--------------------------------------
	// 詳細画面制御
	//--------------------------------------
	kintone.events.on(EVENT_DETAIL_SHOW, function(event) {
		//--------------------------------------
		// 変数宣言
		//--------------------------------------
        var record = event.record;
        
		//--------------------------------------
		// 表示/非表示
		//--------------------------------------
		set_fieldshown(false);

		return event;
		
	});
	
	//--------------------------------------
	// 一覧画面制御
	//--------------------------------------
//	kintone.events.on(EVENT_INDEX_SHOW, function(event) {
//		return event;
//	});
	
	//--------------------------------------
	// 登録/編集画面制御
	//--------------------------------------
	kintone.events.on(EVENT_CREATE_EDIT_SHOW, function(event) {
		//--------------------------------------
		// 変数宣言
		//--------------------------------------
		var record = event.record;

		//--------------------------------------
		// 初期値設定/複製対応
		//--------------------------------------
		if(event.type === 'app.record.create.show'){
		}
		
		//--------------------------------------
		// 表示/非表示
		//--------------------------------------
		set_fieldshown(false);
		
		//--------------------------------------
		// 編集可/不可
		//--------------------------------------
		if(event.type !== 'app.record.create.show'){
		}
		
		//--------------------------------------
		// 住所郵便番号→住所変換ボタン
		//--------------------------------------
		var btn_arr = [
			'sp_self_zip'
		];
		
		for(var i = 0; i < btn_arr.length; i++){
			var button = document.createElement('button');
			button.className = 'button_cstm';//プラグイン向けCSSを流用する場合
			button.id = btn_arr[i];
			button.innerHTML = '住所を取得';
			button.style = 'margin-top: 29px';
			button.onclick = function() {
				var btn_id = $(this).attr('id');
				set_address(event, btn_id);
			};
			kintone.app.record.getSpaceElement(btn_arr[i]).appendChild(button);
		}
		
		return event;
	});
	
	//------------------------------------------------------
	// 一覧編集回避処理
	//------------------------------------------------------
	kintone.events.on(EVENT_INDEX_EDIT_SHOW, function(event) {
		//------------------------------
		// 変数宣言
		//------------------------------
		var record = event.record;
		
		//--------------------------
		// 一覧画面編集回避対応
		//--------------------------
		for(var str in record){
			if(event.record[str]){
				event.record[str]['disabled'] = true;
			}
		}
		
		return event;
	});

})(jQuery);
